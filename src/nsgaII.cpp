// the general include for eo
#include <eo>   // Evolving Object: fundation of EO, evolutionary algorithm
#include <moeo> // Evolving Multiobjective Object
#include <es.h>
#include <DTLZ/src/PolynomialMutation.h>
#include <DTLZ/src/SBXCrossover.h>

// Fitness function
#include "utils/trafficEvalMO.h" // sphere function
#include "utils/bounds.h"

// type of objective vector
typedef moeoRealObjectiveVector<moeoObjectiveVectorTraits> ObjectiveVector;

// type of solution
typedef moeoRealVector<ObjectiveVector> Indi;

int main(int argc, char **argv)
{
    // parameters
    eoParser parser(argc, argv);

    // random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // problem dimension
    size_t d = parser.createParam<size_t>(10, "vecSize", "Problem dimension", 'd', "Problem").value();

    // mutation rate per variable
    double mutpervar_rate = parser.createParam(1.0, "vrate", "Mutation rate per variable", 'm', "Algorithm").value();

    // mutation eta
    double mut_eta = parser.createParam(1.0, "meta", "eta parameter of polynomial mutation", '\0', "Algorithm").value();

    // mutation rate
    double mutation_rate = parser.createParam(1.0, "pmut", "Mutation rate", 'M', "Algorithm").value();

    // xover eta
    double xover_eta = parser.createParam(1.0, "xeta", "eta parameter of SBX crossover", '\0', "Algorithm").value();

    // xover rate
    double xover_rate = parser.createParam(0.9, "xmut", "Crossover rate", 'X', "Algorithm").value();

    // Population size
    uint32_t mu = parser.createParam(20, "popSize", "Population size", 'P', "Algorithm").value();

    // stopping criterium based on time
    time_t duration = parser.createParam(1, "time", "Time limit stopping criterium (number of seconds)", 't', "Algorithm").value();

    // stopping criterium based on number of evaluations
    uint32_t maxEval = parser.createParam(10000, "maxEval", "Number of evaluations stopping criterium", 'e', "Algorithm").value();

    // output file
    std::string fileOutName = parser.createParam(std::string("nsga.csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();

    make_verbose(parser);
    make_help(parser);

    // set the objective vector: false:maximization, true:minimization
    // two objectives to minimize (true for each objective)
    std::vector<bool> objVec(2, true);
    moeoObjectiveVectorTraits::setup(2, objVec);

    TrafficEvalMO<Indi> _eval;

    // to count the number of evalation
    eoEvalFuncCounter<Indi> eval(_eval, "neval");

    // random seed
    rng.reseed(seed);

    // vector of bounds for each variable
    eoRealInitBounded<Indi> init(Bounds::bounds);

    // create a pop of size mu using init for initialization
    eoPop<Indi> pop(mu, init);

    // Continuator for the algorithm
    eoEvalContinue<Indi> evalCont(eval, maxEval);

    eoTimeContinue<Indi> timeCont(duration);

    eoCombinedContinue<Indi> continuator(evalCont);
    continuator.add(timeCont);
    
    // variation operators
    PolynomialMutation<Indi> mutation(Bounds::bounds, mutpervar_rate, mut_eta);

    SBXCrossover<Indi> xover(Bounds::bounds, xover_eta);

    // classic variation operator with mutation, and crossover
    eoSGATransform<Indi> transform(xover, xover_rate, mutation, mutation_rate);

    // checkpoint: substitute continuator to report some statistic, ouput, etc.
    eoCheckPoint<Indi> checkpoint(continuator);

    // State of the search
    eoState outState;

    // parameter of the search (usefull for restart)
    //outState.registerObject(parser);
    // random seed (usefull for restart)
    //outState.registerObject(rng);
    // add population to the state
    outState.registerObject(pop);

    // Update state every 10th generation, and save it into file
    eoCountedStateSaver stateSaver(10, outState, "generation_nsgaII_", "dat");

    // Add the the state to checkpoint to be processed
    checkpoint.add(stateSaver);

    // the optimization algorithm
    moeoNSGAII<Indi> solver(checkpoint, eval, transform);

    // evaluation of the initial population
    for (unsigned i = 0; i < pop.size(); i++)
        eval(pop[i]);

    // run the algorithm
    solver(pop);

    // sort the population according to fitness values (decreasing order)
    //pop.sort();

    //std::cout << eval.value() << " " << neighborEval.value() << std::endl;
    std::cout << pop << std::endl;
}
