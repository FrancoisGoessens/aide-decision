// the general include for eo
#include <eo>   // Evolving Object: fundation of EO, evolutionary algorithm
#include <es.h> // EO: Evolution Strategy part

// Fitness function
#include <sphereEval.h>  // sphere evaluation function
#include <trafficEval.h> // sphere evaluation function

// Differential Evolution
#include "utils/deBreed.h"
#include "utils/deRand1Mutation.h"
#include "utils/deStandardXover.h"
#include "utils/deReplacement.h"
#include "utils/bounds.h"

// type of the representations
typedef eoMinimizingFitness Fitness;
typedef eoReal<Fitness> Indi;

int main(int argc, char **argv)
{
    // parameters
    eoParser parser(argc, argv);

    // random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // bit string size
    size_t n = parser.createParam<size_t>(24, "vecSize", "Problem dimension", 'n', "Problem").value();

    // Population size
    uint32_t mu = parser.createParam(100, "popSize", "Population size", 'P', "Algorithm").value();

    // mutation factor
    double mutationFactor = parser.createParam(0.8, "mutationFactor", "Mutation factor", 'F', "Algorithm").value();

    // xover rate
    double xoverRate = parser.createParam(0.9, "xoverRate", "CrossOver rate (CR)", 'X', "Algorithm").value();

    // stopping criterium based on time
    time_t duration = parser.createParam(1, "time", "Time limit stopping criterium (number of seconds)", 't', "Algorithm").value();

    // stopping criterium based on number of evaluations
    uint32_t maxEval = parser.createParam(1000, "maxEval", "Number of evaluations stopping criterium", 'e', "Algorithm").value();

    // Create param eval for time or CO2
    int evalType = parser.createParam(1, "evalType", "Time => 1 :: CO2 => 2", 'F', "Algorithm").value();

    // output file
    std::string fileOutName = parser.createParam(std::string("DE_"+ std::to_string(evalType) + ".csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();

    make_verbose(parser);
    make_help(parser);

    // my own fitness function: traffic function
    TrafficEval<Indi> _eval(evalType);

    // to count the number of evalation
    eoEvalFuncCounter<Indi> eval(_eval, "neval");

    // fitness value to reach
    double targetedFitness = 0;
    
    // random seed
    rng.reseed(seed);

    // initialisation of one single gene (real value between -5 and 5)
    eoRealInitBounded<Indi> init(Bounds::bounds);

    // Continuator for the ES
    eoEvalContinue<Indi> evalCont(eval, maxEval);
    eoTimeContinue<Indi> timeCont(duration);
    eoFitContinue<Indi> fitnessCont(targetedFitness);
    eoCombinedContinue<Indi> continuator(timeCont);
    //continuator.add(evalCont);
    continuator.add(fitnessCont);

    // Create a pop of size mu
    eoPop<Indi> pop;
    pop.resize(mu);

    // DE mutation
    DeRand1Mutation<Indi> mutation(pop, mutationFactor, Bounds::bounds);

    // DE crossover
    DEStandardXover<Indi> xover(pop, xoverRate);

    // DE Breed
    DEBreed<Indi> breed(mutation, xover);

    // One to One replacement if better
    DEReplacement<Indi> replace;

    // checkpoint: substitute continuator to report some statistic, ouput, etc.
    eoCheckPoint<Indi> checkpoint(continuator);

    // output into a file: file name, separator, keep file, header
    eoFileMonitor monitor(fileOutName, ",", false, true);

    // add the monitor to the checkpoint to be processed
    checkpoint.add(monitor);

    // Create a counter parameter
    eoValueParam<unsigned> generationCounter(0, "iteration");
    // Increment this counter at each generation
    eoIncrementor<unsigned> increment(generationCounter.value());

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(increment);

    // time counter
    eoTimeCounter timeStat;
    checkpoint.add(timeStat);

    // best fitness in population
    eoBestFitnessStat<Indi> bestStat("best");

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(bestStat);

    // second moment stats: average and stdev
    eoSecondMomentStats<Indi> avgStdStat("avg std");
    checkpoint.add(avgStdStat);

    // report some statistics into the file
    monitor.add(generationCounter); // iteration value
    monitor.add(timeStat);          // time report
    monitor.add(eval);              // number of evaluations
    monitor.add(bestStat);          // best fitness in the population
    monitor.add(avgStdStat);        // avg and std fitness in the population

    // Differential evolution algorithm
    eoEasyEA<Indi> solver(checkpoint, eval, breed, replace);

    // evaluation of the initial population
    for (unsigned i = 0; i < pop.size(); i++)
    {
        init(pop[i]);
        eval(pop[i]);
    }

    // run the algorithm
    solver(pop);

    // sort the population according to fitness values (decreasing order)
    pop.sort();

    //std::cout << eval.value() << " " << neighborEval.value() << std::endl;
    std::cout << pop.at(0) << std::endl;
}
