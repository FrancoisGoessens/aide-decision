// the general include for eo
#include <eo> // Evolving Object: fundation of EO, evolutionary algorithm
#include <edo>
#include <es.h>

// Fitness function
#include "utils/sphereEval.h"  // sphere function
#include "utils/trafficEval.h" // sphere function

// Bornes solution.
#include "utils/bounds.h"
#include "utils/customRepairer.h"

// type of the representations
typedef eoMinimizingFitness Fitness; // Fitness type, double minimzation problem
typedef eoReal<Fitness> Indi;        // bit string with double fitness value

using CMA = edoNormalAdaptive<Indi>;

int main(int argc, char **argv)
{

    // parameters
    eoParser parser(argc, argv);

    // random seed
    uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();

    // problem dimension
    size_t d = parser.createParam<size_t>(24, "vecSize", "Problem dimension", 'd', "Problem").value();

    // Population size
    uint32_t mu = parser.createParam(100, "popSize", "Population size", 'P', "Algorithm").value();

    // stopping criterium based on time
    time_t duration = parser.createParam(1, "time", "Time limit stopping criterium (number of seconds)", 't', "Algorithm").value();

    // stopping criterium based on number of evaluations
    uint32_t maxEval = parser.createParam(10000, "maxEval", "Number of evaluations stopping criterium", 'e', "Algorithm").value();

    // Create param eval for time or CO2
    int evalType = parser.createParam(1, "evalType", "Time => 1 :: CO2 => 2", 'F', "Algorithm").value();

    // output file
    std::string fileOutName = parser.createParam(std::string("cmaES_"+ std::to_string(evalType) + ".csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();


    make_verbose(parser);
    make_help(parser);

    // fitness function
    TrafficEval<Indi> _eval(evalType);

    // to count the number of evalation
    eoEvalFuncCounter<Indi> eval(_eval, "neval");

    // fitness value to reach (if known)
    double targetedFitness = 1e-8;

    // evaluation function of the population
    eoPopLoopEval<Indi> pop_eval(eval);

    // random seed
    rng.reseed(seed);

    // initialisation of one single gene
    eoRealInitBounded<Indi> init(Bounds::bounds);

    // Continuator for the ES
    eoEvalContinue<Indi> evalCont(eval, maxEval);

    eoTimeContinue<Indi> timeCont(duration);

    eoFitContinue<Indi> fitnessCont(targetedFitness); // if known

    eoCombinedContinue<Indi> continuator(evalCont);
    continuator.add(timeCont);
    continuator.add(fitnessCont);

    // CMA : stop NaN in matrix
    edoContAdaptiveFinite<CMA> distrib_continue;

    // selection of EA
    eoRankMuSelect<Indi> select(d / 2);

    // Co-variance matrix variation
    edoNormalAdaptive<Indi> gaussian(d);
    edoEstimatorNormalAdaptive<Indi> estimator(gaussian);
    CustomRepairer<Indi> repairer(Bounds::bounds);
    edoSamplerNormalAdaptive<Indi> sampler(repairer);

    // (mu, lambda)-Evolution Strategy replacement
    eoCommaReplacement<Indi> replace;

    // checkpoint: substitute continuator to report some statistic, ouput, etc.
    eoCheckPoint<Indi> checkpoint(continuator);

    // output into a file: file name, separator, keep file, header
    eoFileMonitor monitor(fileOutName, ",", false, true);

    // add the monitor to the checkpoint to be processed
    checkpoint.add(monitor);

    // Create a counter parameter
    eoValueParam<unsigned> generationCounter(0, "iteration");
    // Increment this counter at each generation
    eoIncrementor<unsigned> increment(generationCounter.value());

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(increment);

    // time counter
    eoTimeCounter timeStat;
    checkpoint.add(timeStat);

    // best fitness in population
    eoBestFitnessStat<Indi> bestStat("best");

    // add to the checkpoint to be processed, and monitor to be reported
    checkpoint.add(bestStat);

    // second moment stats: average and stdev
    eoSecondMomentStats<Indi> avgStdStat("avg std");
    checkpoint.add(avgStdStat);

    // report some statistics into the file
    monitor.add(generationCounter); // iteration value
    monitor.add(timeStat);          // time report
    monitor.add(eval);              // number of evaluations
    monitor.add(bestStat);          // best fitness in the population
    monitor.add(avgStdStat);        // avg and std fitness in the population

    // the optimization algorithm
    edoAlgoAdaptive<CMA> solver(gaussian, pop_eval, select,
                                estimator, sampler, replace,
                                checkpoint, distrib_continue);

    // create a pop of size mu using init for initialization
    eoPop<Indi> pop(mu, init);

    // evaluation of the initial population
    for (unsigned i = 0; i < pop.size(); i++)
        eval(pop[i]);

    // run the algorithm
    solver(pop);

    // sort the population according to fitness values (decreasing order)
    pop.sort();

    //std::cout << eval.value() << " " << neighborEval.value() << std::endl;
    std::cout << pop.at(0) << std::endl;
}
