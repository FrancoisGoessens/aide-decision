#ifndef _DEStandardXover_h
#define _DEStandardXover_h

#include "deOp.h"

/* Classe permettant de gérer les remplacements de solution dans la population. */
template <class EOT> class DEStandardXover : public DEOp<EOT> {
  using DEOp<EOT>::parents;
  using DEOp<EOT>::id;

  protected:
    // crossover rate
    double CR;
  
  public:
    DEStandardXover(eoPop<EOT> & _parents, double _CR) : DEOp<EOT>(_parents), CR(_CR){}

    /* Xover of the solution */
    virtual bool operator()(EOT & _solution) {
      unsigned jrand = rng.random(_solution.size());

      for(unsigned j = 0; j < jrand; j++) {
        if (rng.uniform() > CR) _solution[j] = parents[id][j];
      }

      for(unsigned j = 0; j < jrand + 1; j++) {
        if (rng.uniform() > CR) _solution[j] = parents[id][j];
      }

      _solution.invalidate();

      return true;
    }
};
#endif
