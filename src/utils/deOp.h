#ifndef _DEOp_h
#define _DEOp_h

/* Opérateur paradiseo. */
template <class EOT> class DEOp : public eoMonOp<EOT> {
  public:
    DEOp(eoPop<EOT> & _parents) : parents(_parents) {
      id = 0;
    }

    /* Set the population of parents */
    void pop(const eoPop<EOT> & _parents) {
      parents = _parents;
    }

    /* Set the index of the parents to transform */
    void index(unsigned _id) {
      id = _id;
    }

  protected:
    eoPop<EOT> & parents;
    unsigned id;
};
#endif
