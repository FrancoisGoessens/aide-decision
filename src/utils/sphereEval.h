/*
    <sphereEval.h>

    Exercice 2 of tutorial

    Author: Verel Sébastien
    Date: 2021/12/06 V0
*/

#ifndef _sphereEval_h
#define _sphereEval_h

#include <eoEvalFunc.h>

// Full evaluation Function for exercice 2
template< class EOT > class SphereEval : public eoEvalFunc<EOT> {
  public:
	/**
	 * Sphere fitness function
      *
	 * @param _sol the solution to evaluate
	 */
    void operator() (EOT& _sol) {
      double sum = 0;
       
      for (unsigned int i = 0; i < _sol.size() - 1; i++) {
        sum += _sol[i] * _sol[i];
      }
       
      _sol.fitness(sum);
    }
};
#endif
