#ifndef _Bounds_h
#define _Bounds_h

#include <eo>
#include <vector>

class Bounds{
	public:
		inline static std::vector<double> lb {1.2, 0.1, 1.2, 0.1, 0.5, 1.2, 0.5, 0.7, 0.1, 0.2, 0.7, 0.7, 20, 10, 20, 25, 10, 25, 25, 25, 20, 10, 30, 30};
		inline static std::vector<double> up {1.8, 0.3, 1.8, 0.3, 1, 1.8, 1, 1.3, 0.3, 0.4, 1.3, 1.3, 30, 20, 30, 35, 20, 35, 35, 35, 30, 20, 35, 35};
		inline static eoRealVectorBounds bounds = eoRealVectorBounds(Bounds::lb, Bounds::up);
};
#endif
