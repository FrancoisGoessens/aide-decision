#ifndef _DEReplacement_h
#define _DEReplacement_h

#include <eoPop.h>
#include <eoReplacement.h>

/** Classe permettant de gérer les remplacements de solution dans la population. */
template <class EOT> class DEReplacement : public eoReplacement<EOT> {
  public:
    DEReplacement() { }
 
    void operator()(eoPop<EOT>& _parents, eoPop<EOT>& _offspring) {
      for(unsigned i = 0; i < _parents.size(); i++) {
        // replace if better or equal
        if (_parents[i].fitness() <= _offspring[i].fitness()) _parents[i] = _offspring[i] ;
      }
    }
};
#endif
