#ifndef _eoCombinedMonOp_H
#define _eoCombinedMonOp_H

#include "eoOp.h"
#include "utils/eoLogger.h"

/*Combined different MonOp, 
  and apply them iteratively on solution

  Remember that a eoMonOp could be a moLocalSearch
  Usefull to define Hybrid EA*/

template <class EOT> class eoCombinedMonOp: public eoMonOp<EOT> {

  protected:
    // list of MonOp to apply
    std::vector<eoMonOp<EOT>*> ops;
  
  public:
    // Constructor with the first MonOp to apply
    eoCombinedMonOp(eoMonOp<EOT> & _first)
    {
      ops.push_back(&_first);
    }

    virtual void add(eoMonOp<EOT> & _op)
    {
      ops.push_back(&_op);
    }

    virtual bool operator()(EOT & _solution)
    {
      bool ok = (*ops[0])(_solution);

      for(unsigned i = 1; i < ops.size(); i++)
        ok = (*ops[i])(_solution) || ok;

      return ok;
    }

    // outputs the operators
    virtual void printOn(std::ostream & _os)
    {
      _os << "In " << className() << "\n" ;
      for (unsigned i = 0; i < ops.size(); i++)
        _os << ops[i]->className() << "\n";
    }

    virtual std::string className() const { return "eoCombinedMonOp"; }
};
#endif
