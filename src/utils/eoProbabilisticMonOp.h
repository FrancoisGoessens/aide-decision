#ifndef _eoProbabilisticMonOp_H
#define _eoProbabilisticMonOp_H

#include "eoOp.h"
#include "utils/eoLogger.h"

// Apply a MonOp according to a Bernouilli law
template <class EOT> class eoProbabilisticMonOp: public eoMonOp<EOT> {
  protected:
    // MonOp to apply
    eoMonOp<EOT> & op;
    // rate of the Bernouilli law
    double rate;

  public:
    //Constructor with the first MonOp to apply
    eoProbabilisticMonOp(eoMonOp<EOT> & _op, double _rate) : op(_op), rate(_rate) {}

    //Apply the MonOp according to rate
    virtual bool operator()(EOT & _solution) {
      bool ok = false ;

      if (rng.uniform() < rate) ok = op(_solution);

      return ok;
    }

    // outputs the operators
    virtual void printOn(std::ostream & _os) {
      _os << "In " << className() << "\n" ;
      _os << op.className() << " with rate " << rate << "\n";
    }

    virtual std::string className() const { return "eoProbabilisticMonOp"; }
};

#endif
