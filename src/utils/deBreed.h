#ifndef _DEBreed_h
#define _DEBreed_h

#include "eoBreed.h"
#include "deOp.h"

/** Classe permettant de gérer les variations appliquées sur une population (mutation et croisement). */
template <class EOT> class DEBreed : public eoBreed<EOT> {
  public:
    DEBreed(DEOp<EOT> & _mutation, DEOp<EOT> & _xover) : mutation(_mutation), xover(_xover){}

    void operator()(const eoPop<EOT>& _parents, eoPop<EOT>& _offspring) {
      _offspring.resize(_parents.size());
      mutation.pop(_parents);
      xover.pop(_parents);

      for(unsigned i = 0; i < _offspring.size(); i++) {
        mutation.index(i);
        mutation(_offspring[i]);
        xover.index(i);
        xover(_offspring[i]);
      }
    }

  private:
    DEOp<EOT> & mutation;
    DEOp<EOT> & xover;
};
#endif
