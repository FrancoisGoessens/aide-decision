#ifndef _CustomBounder_cpp
#define _CustomBounder_cpp

#include <edoRepairer.h>
#include <eo>

/** Classe permettant d'appliquer une réparation sur une solution
 * Utilisé par l'algorithme cmaES. */
template <typename EOT> class CustomRepairer : public edoRepairer<EOT>
{
	public:
		CustomRepairer(eoRealVectorBounds bounds): edoRepairer<EOT>(), bounds(bounds){}
		void operator()( EOT& sol )
		{
			for (unsigned int d = 0; d < sol.size(); ++d) {
				if(sol[d] < bounds[d]->minimum())
					sol[d] = bounds[d]->minimum();
				else if(sol[d] > bounds[d]->maximum())
					sol[d] = bounds[d]->maximum();
			}
		}
	private:
		eoRealVectorBounds bounds;
};
#endif
