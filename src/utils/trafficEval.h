#ifndef _trafficEval_h
#define _trafficEval_h

#include <eoEvalFunc.h>

#include <artis-star/common/RootCoordinator.hpp>
#include "../traffic/defs.hpp"

// Full evaluation Function for exercice 2
template <class EOT> class TrafficEval : public eoEvalFunc<EOT> {
  private :
    int eval;

  public:
    TrafficEval(int eval) : eval(eval){}

    /**
	 * Traffic fitness function
      *
	 * @param _sol the solution to evaluate
	 */
    void operator()(EOT &_sol) {
      std::vector<double> result = evaluate(_sol);
      if( eval == 1 ) _sol.fitness(result[1]); // TIME 
      else if (eval == 2) _sol.fitness(result[2]); // CO2
    }

    std::vector<double> evaluate(EOT &solution) {
      SimpleNetworkParameters parameters = {{0, 1, 3, 2, .1, 432451, {{0, 0, 0}, {0, 1, 0}}, {0.8, 0.2}},
                                            {10000, 0.2, 0.4, 0.3, .02, 432452, {{0, 0, 0}, {0, 1, 0}}, {0.8, 0.2}},
                                            {20000, 1, 3, 2, .1, 432453, {{0, 0, 0}, {0, 1, 0}}, {0.8, 0.2}},
                                            {850, 2, solution.at(12), 0.12, 6, solution.at(0), 2, 1},
                                            {325, 1, solution.at(13), 0.12, 5.78, solution.at(1), 1, 2},
                                            {650, 1, solution.at(14), 0.14, 3.78, solution.at(2), 0, 3},
                                            {300, 2, solution.at(15), 0.12, 5.78, solution.at(3), 2, 4},
                                            {385, 1, solution.at(16), 0.14, 3.78, solution.at(4), 1, 5},
                                            {470, 2, solution.at(17), 0.12, 5.78, solution.at(5), 1, 6},
                                            {520, 2, solution.at(18), 0.12, 5.78, solution.at(6), 0, 7},
                                            {310, 2, solution.at(19), 0.12, 5.78, solution.at(7), 2, 8},
                                            {790, 1, solution.at(20), 0.12, 5.78, solution.at(8), 1, 9},
                                            {740, 1, solution.at(21), 0.14, 6, solution.at(9), 0, 10},
                                            {270, 2, solution.at(22), 0.12, 5.78, solution.at(10), 1, 11},
                                            {700, 2, solution.at(23), 0.12, 5.78, solution.at(11), 0, 12},
                                            {0., 1, 2, {1}},
                                            {0., 1, 1, {1}},
                                            {0., 2, 1, {0.25, 0.75}},
                                            {0., 1, 2, {1}},
                                            {0., 1, 2, {1}},
                                            {0., 2, 1, {0.5, 0.5}}};
      artis::common::context::Context<artis::common::DoubleTime> context(25200, 27000);
      artis::common::RootCoordinator<
        artis::common::DoubleTime, artis::pdevs::Coordinator<
                                          artis::common::DoubleTime,
                                          SimpleNetworkGraphManager,
                                          SimpleNetworkParameters>>
          rc(context, "root", parameters, artis::common::NoParameters());
      rc.attachView("System", new TwoLinkWithNodeView);
      rc.switch_to_timed_observer(100);
      rc.run(context);
      const TwoLinkWithNodeView::View &view = rc.observer().view("System");
      auto generator_1_values = view.get<unsigned int>("Generator_1:counter");
      auto generator_8_values = view.get<unsigned int>("Generator_8:counter");
      auto generator_11_values = view.get<unsigned int>("Generator_11:counter");
      auto link_1_wd_values = view.get<double>("Link_1:waiting_duration_sum");
      auto link_2_wd_values = view.get<double>("Link_2:waiting_duration_sum");
      auto link_3_wd_values = view.get<double>("Link_3:waiting_duration_sum");
      auto link_4_wd_values = view.get<double>("Link_4:waiting_duration_sum");
      auto link_5_wd_values = view.get<double>("Link_5:waiting_duration_sum");
      auto link_6_wd_values = view.get<double>("Link_6:waiting_duration_sum");
      auto link_7_wd_values = view.get<double>("Link_7:waiting_duration_sum");
      auto link_8_wd_values = view.get<double>("Link_8:waiting_duration_sum");
      auto link_9_wd_values = view.get<double>("Link_9:waiting_duration_sum");
      auto link_10_wd_values = view.get<double>("Link_10:waiting_duration_sum");
      auto link_11_wd_values = view.get<double>("Link_11:waiting_duration_sum");
      auto link_12_wd_values = view.get<double>("Link_12:waiting_duration_sum");
      auto counter_2_values = view.get<unsigned int>("Counter_2:counter");
      auto counter_4_values = view.get<unsigned int>("Counter_4:counter");
      auto counter_7_values = view.get<unsigned int>("Counter_7:counter");
      auto counter_12_values = view.get<unsigned int>("Counter_12:counter");
      auto counter_2_d_values = view.get<double>("Counter_2:travel_duration_sum");
      auto counter_4_d_values = view.get<double>("Counter_4:travel_duration_sum");
      auto counter_7_d_values = view.get<double>("Counter_7:travel_duration_sum");
      auto counter_12_d_values = view.get<double>("Counter_12:travel_duration_sum");
      auto counter_2_dm_values = view.get<double>("Counter_2:travel_duration_min");
      auto counter_4_dm_values = view.get<double>("Counter_4:travel_duration_min");
      auto counter_7_dm_values = view.get<double>("Counter_7:travel_duration_min");
      auto counter_12_dm_values = view.get<double>("Counter_12:travel_duration_min");
      auto counter_2_dmm_values = view.get<double>("Counter_2:travel_duration_max");
      auto counter_4_dmm_values = view.get<double>("Counter_4:travel_duration_max");
      auto counter_7_dmm_values = view.get<double>("Counter_7:travel_duration_max");
      auto counter_12_dmm_values = view.get<double>("Counter_12:travel_duration_max");
      auto counter_2_c_values = view.get<double>("Counter_2:consumption_sum");
      auto counter_4_c_values = view.get<double>("Counter_4:consumption_sum");
      auto counter_7_c_values = view.get<double>("Counter_7:consumption_sum");
      auto counter_12_c_values = view.get<double>("Counter_12:consumption_sum");
      auto counter_2_cm_values = view.get<double>("Counter_2:consumption_min");
      auto counter_4_cm_values = view.get<double>("Counter_4:consumption_min");
      auto counter_7_cm_values = view.get<double>("Counter_7:consumption_min");
      auto counter_12_cm_values = view.get<double>("Counter_12:consumption_min");
      auto counter_2_cmm_values = view.get<double>("Counter_2:consumption_max");
      auto counter_4_cmm_values = view.get<double>("Counter_4:consumption_max");
      auto counter_7_cmm_values = view.get<double>("Counter_7:consumption_max");
      auto counter_12_cmm_values = view.get<double>("Counter_12:consumption_max");

      unsigned int g_n_1 = generator_1_values.back().second, g_n_8 = generator_8_values.back().second, g_n_11 = generator_11_values.back().second;
      double c_n_2_d = counter_2_d_values.back().second, c_n_4_d = counter_4_d_values.back().second,
             c_n_7_d = counter_7_d_values.back().second, c_n_12_d = counter_12_d_values.back().second;
      double c_n_2_c = counter_2_c_values.back().second, c_n_4_c = counter_4_c_values.back().second,
             c_n_7_c = counter_7_c_values.back().second, c_n_12_c = counter_12_c_values.back().second;
      double l_wd_n_1 = link_1_wd_values.back().second, l_wd_n_2 = link_2_wd_values.back().second,
             l_wd_n_3 = link_3_wd_values.back().second, l_wd_n_4 = link_4_wd_values.back().second,
             l_wd_n_5 = link_5_wd_values.back().second, l_wd_n_6 = link_6_wd_values.back().second,
             l_wd_n_7 = link_7_wd_values.back().second, l_wd_n_8 = link_8_wd_values.back().second,
             l_wd_n_9 = link_9_wd_values.back().second, l_wd_n_10 = link_10_wd_values.back().second,
             l_wd_n_11 = link_11_wd_values.back().second, l_wd_n_12 = link_12_wd_values.back().second;
       std::vector<double> result;
       result.push_back(l_wd_n_1 + l_wd_n_2 + l_wd_n_3 + l_wd_n_4 + l_wd_n_5 + l_wd_n_6 + l_wd_n_7 + l_wd_n_8 + l_wd_n_9 + l_wd_n_10 + l_wd_n_11 + l_wd_n_12); // waiting duration
       result.push_back(c_n_2_d + c_n_4_d + c_n_7_d + c_n_12_d);                                                                                               // duration
       result.push_back(c_n_2_c + c_n_4_c + c_n_7_c + c_n_12_c);                                                                                               // co2 emission
       result.push_back(g_n_1 + g_n_8 + g_n_11);                                                                                                               // generated vehicle number
       return result;
    }
};
#endif
